package com.javacodegeeks.gpscollector;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import android.os.Environment;
import android.provider.Settings;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ScrollingActivity extends AppCompatActivity {

    private LocationManager locationManager;
    private LocationListener listener;
    private int count;
    private TextView t;
    private TextView d;
    private TextView i;
    private Calendar calendar;
    private SimpleDateFormat dateFormat;
    private String date;
    private int interval;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);

        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        date = dateFormat.format(calendar.getTime());
        interval = 5000;


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1000);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        d = (TextView) findViewById(R.id.date);
        i =(TextView) findViewById(R.id.interval);
        t = (TextView) findViewById(R.id.t);

        i.append(" " + (interval/1000)+"s");
        d.append(" " + dateFormat.format(calendar.getTime()));

        FloatingActionButton mail = (FloatingActionButton) findViewById(R.id.mail);
        mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String filename = "data "+date;
                String file = d.getText().toString()+"\n"+i.getText().toString()+"\n"+t.getText().toString();
                save(filename,file);
                sendData();
            }
        });

        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                count++;
                t.append("\n " + count + ".  lon:" + new DecimalFormat("0.0000").format(location.getLongitude())
                        + " - lat:" + new DecimalFormat("0.0000").format(location.getLatitude()));
            }
            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
            }
            @Override
            public void onProviderEnabled(String s) {
            }
            @Override
            public void onProviderDisabled(String s) {
                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i);
            }
        };
        getLocation();
    }


    void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET}
                        , 10);
            }
            return;
        }
        locationManager.requestLocationUpdates("gps", interval, 0, listener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_save) {
            String filename = "data "+date;
            String file = d.getText().toString()+"\n"+i.getText().toString()+"\n"+t.getText().toString();
            save(filename,file);
            restart();

            t.setText(R.string.text);
            count = 0;
            return true;
        }
        if(id == R.id.action_refresh) {
            restart();
            return true;
        }

        if (id == R.id.action_quit) {
            System.exit(0);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void sendData() {
        Intent in = new Intent(Intent.ACTION_SEND);
        in.setType("message/rfc822");
        in.putExtra(Intent.EXTRA_EMAIL  , new String[]{""});
        in.putExtra(Intent.EXTRA_SUBJECT, "coordinates");
        in.putExtra(Intent.EXTRA_TEXT   , d.getText().toString()+"\n"+i.getText().toString()+"\n"+t.getText().toString());

        try {
            startActivity(Intent.createChooser(in, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(ScrollingActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }

    }

    public void save(String filename, String content)  {

        String root = Environment.getExternalStorageDirectory().getAbsolutePath()+ "/GPSCollector";

        File myDir = new File(root);

        if(!myDir.exists() && !myDir.isDirectory()){
            myDir.mkdirs();
        }

        File file = new File(root,filename+ ".txt");

        try {
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(content.getBytes());
            fos.close();
            Toast.makeText(this, "Saved! ",Toast.LENGTH_LONG).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(this,"File not found! ", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this,"Error saving! ", Toast.LENGTH_SHORT).show();
        }

    }

    public void restart(){
        Intent intent = new Intent(this, ScrollingActivity.class);
        this.startActivity(intent);
        this.finishAffinity();
    }

}
